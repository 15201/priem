/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.crypto;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.messages.*;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class Decoder extends ReplayingDecoder<Message> {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(Decoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        logger.debug("gone");
        out.add(decode(in.readBytes(in.readInt())));
    }

    private Message decode(ByteBuf data) {
        Gson gson = new Gson();
        String json = decodeString(data).toString(StandardCharsets.UTF_8);
        logger.debug("<- " + json);
        HashMap message = gson.fromJson(json, HashMap.class);
        switch ((String)message.get("type")) {
            case ("SignUpInfo"): {
                return gson.fromJson(json, SignUpInfo.class);
            }
            case ("SignInInfo"): {
                return gson.fromJson(json, SignInInfo.class);
            }
            case ("ChangePasswordMessage"): {
                return gson.fromJson(json, ChangePasswordMessage.class);
            }
            case ("SearchRequest"): {
                return gson.fromJson(json, SearchRequest.class);
            }
            case ("ChatMessage"): {
                return gson.fromJson(json, ChatMessage.class);
            }
            case ("requestMessage"): {
                return gson.fromJson(json, RequestMessage.class);
            }
            case ("RequestUser"): {
                return gson.fromJson(json, RequestUser.class);
            }
            case ("Acknowledgement"): {
                return gson.fromJson(json, Acknowledgement.class);
            }
            case ("UserList"): {
                return gson.fromJson(json, UserList.class);
            }
            case ("MessageList"): {
                return gson.fromJson(json, MessageList.class);
            }
        }
        return null;
    }

    private ByteBuf decodeString(ByteBuf string) {
        return string;
    }

}
