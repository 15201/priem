/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.crypto;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class EncodePass {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(EncodePass.class);

    public String encode(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.info(e.getLocalizedMessage(), e);
            System.exit(1);
        }
        return new String(Base64.getEncoder().encode(digest.digest(password.getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8);
    }

}
