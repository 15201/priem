/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.crypto;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.messages.Message;

import java.nio.charset.StandardCharsets;

public class Encoder extends MessageToByteEncoder<Message> {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(Encoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx,
                          Message msg, ByteBuf out) {
        logger.debug("encoded");
        byte[] string = encode(msg);
        logger.debug("-> " + new String(string, StandardCharsets.UTF_8));
        out.writeInt(string.length);
        out.writeBytes(string);
    }

    private byte[] encode(Message message) {
        Gson gson = new Gson();
        byte[] json = gson.toJson(message).getBytes(StandardCharsets.UTF_8);
        return encodeString(json);
    }

    private byte[] encodeString(byte[] string) {
        return string;
    }

}
