/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.settings;

public interface SettingsListener {

    void printError(String error);

    void goToSignIn();

    void goBack(int id);

}
