/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.signIn;

public interface SignInListener {

    void printError(String error);

    void signUp();

    void goToSelectChat(int id);

}
