/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.signIn.SignInUI;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {
    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(Main.class);


    private static volatile String IP;
    private static final int port = 9000;

    public static void main(String[] args) {
        Properties property = new Properties();
        try (FileInputStream fis = new FileInputStream("config.properties")) {
            property.load(fis);
            IP = property.getProperty("local.ip");
        } catch (IOException e) {
            logger.info(e.getLocalizedMessage(), e);
        }
        ClientIO.getDefault().bind(IP, port);
        SignInUI signInUI = new SignInUI();
        signInUI.pack();
        signInUI.setVisible(true);
    }

}
