/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.signIn;

import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.clientIO.ClientIOListener;
import ru.nsu.ccfit.priem.crypto.EncodePass;
import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.Message;
import ru.nsu.ccfit.priem.messages.SignInInfo;

class SignIn implements ClientIOListener {

    private SignInListener listener;

    SignIn() {
        ClientIO.getDefault().setClientIOListener(this);
    }

    void signIn(String login, String password) {
        ClientIO.getDefault().send(new SignInInfo(login, new EncodePass().encode(password)));
    }

    void signUp() {
        listener.signUp();
    }

    void setListener(SignInListener listener) {
        this.listener = listener;
    }

    @Override
    public void callback(Message message) {
        Acknowledgement ack = (Acknowledgement)message;
        if (ack.isAck()) {
            listener.goToSelectChat(Integer.parseInt(ack.getAnswer()));
        } else {
            listener.printError(ack.getAnswer());
        }
    }
}
