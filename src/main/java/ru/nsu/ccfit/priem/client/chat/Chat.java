/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.chat;

import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.clientIO.ClientIOListener;
import ru.nsu.ccfit.priem.messages.ChatMessage;
import ru.nsu.ccfit.priem.messages.Message;
import ru.nsu.ccfit.priem.messages.MessageList;
import ru.nsu.ccfit.priem.messages.RequestMessage;

public class Chat implements ClientIOListener {

    private ChatListener listener;
    private int myID;
    private int notMyID;
    private Thread receiver;

    public Chat(int myID     , int notMyID) {
        this.myID = myID;
        this.notMyID = notMyID;
        ClientIO.getDefault().setClientIOListener(this);
    }

    void getMessagesFirstTime() {
        ClientIO.getDefault().send(new RequestMessage(myID, notMyID, true));
    }

    void runReciever() {
        receiver = new Thread(new Receiver());
        receiver.setDaemon(true);
        receiver.start();
    }

    void stopReciever() {
        receiver.interrupt();
    }

    void sendMessage(ChatMessage message) {
        ClientIO.getDefault().send(message);
    }

    public ChatListener getListener() {
        return listener;
    }

    public void setListener(ChatListener listener) {
        this.listener = listener;
    }

    public int getMyID() {
        return myID;
    }

    public int getNotMyID() {
        return notMyID;
    }

    private class Receiver implements Runnable {

        @Override
        public void run() {
            while (true) {
                ClientIO.getDefault().send(new RequestMessage(myID, notMyID, false));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }

    }

    @Override
    public void callback (Message msg) {
        if (!(msg instanceof MessageList)) {
            return;
        }
        MessageList messageList = (MessageList)msg;
        for (ChatMessage message : messageList.getMessageList()) {
            if (!message.getContent().equals("")) {
                ClientIO.getDefault().getFile(message.getContent()); // fixme dumb file downloading every time
            }
            if (message.getIdSender() == myID) {
                listener.showMyTextMessage(message);
            } else {
                listener.showNotMyTextMessage(message);
            }
        }
    }
}
