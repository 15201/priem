/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.clientIO;

import ru.nsu.ccfit.priem.messages.Message;

public interface ClientIOListener {

    void callback(Message message);

}
