/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.signUp;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.clientIO.ClientIOListener;
import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.Message;
import ru.nsu.ccfit.priem.messages.SignUpInfo;

class SignUp implements ClientIOListener {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(SignUp.class);

    private SignUpListener listener;

    SignUp() {
        ClientIO.getDefault().setClientIOListener(this);
    }

    void signUp(String login, String email) {
        ClientIO.getDefault().send(new SignUpInfo(login, email));
    }

    void setListener(SignUpListener listener) {
        this.listener = listener;
    }

    @Override
    public void callback(Message message) {
        Acknowledgement ack = (Acknowledgement) message;
        if (ack == null) {
            logger.debug("caught null");
            return;
        }
        if (ack.isAck()) {
            listener.goToSignIn();
        } else {
            listener.printError(ack.getAnswer());
        }
    }
}
