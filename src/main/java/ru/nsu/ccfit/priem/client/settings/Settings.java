/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.settings;

import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.clientIO.ClientIOListener;
import ru.nsu.ccfit.priem.crypto.EncodePass;
import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.ChangePasswordMessage;
import ru.nsu.ccfit.priem.messages.Message;

class Settings implements ClientIOListener {

    private SettingsListener listener;
    private int id;

    Settings(int id) {
        this.id = id;
        ClientIO.getDefault().setClientIOListener(this);
    }

    void changeSignedIn() {
        listener.goToSignIn();
    }

    void goBack() { listener.goBack(id); }

    void changePassword(String oldPassword, String newPassword) {
        EncodePass encodePass = new EncodePass();
        ClientIO.getDefault().send(new ChangePasswordMessage(id, encodePass.encode(oldPassword), encodePass.encode(newPassword)));
    }

    void setListener(SettingsListener listener) {
        this.listener = listener;
    }

    @Override
    public void callback(Message message) {
        Acknowledgement ack = (Acknowledgement)message;
        if (ack.isAck()) {
            listener.goToSignIn();
        } else {
            listener.printError(ack.getAnswer());
        }
    }
}
