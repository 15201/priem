/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.selectChat;

import ru.nsu.ccfit.priem.client.clientIO.ClientIO;
import ru.nsu.ccfit.priem.client.clientIO.ClientIOListener;
import ru.nsu.ccfit.priem.messages.Message;
import ru.nsu.ccfit.priem.messages.RequestUser;
import ru.nsu.ccfit.priem.messages.SearchRequest;
import ru.nsu.ccfit.priem.messages.UserList;

class SelectChat implements ClientIOListener {

    private SelectChatListener listener;
    private int idUser;

    SelectChat(int idUser) {
        this.idUser = idUser;
        ClientIO.getDefault().setClientIOListener(this);
    }

    void getUsersFirstTime() {
        ClientIO.getDefault().send(new RequestUser(idUser));
    }

    void chooseChat(int idUser) {
        listener.goToChat(this.idUser, idUser);
    }

    void search(String userName) {
        ClientIO.getDefault().send(new SearchRequest(userName));
    }

    void goToSettings() {
        listener.goToSettings(idUser);
    }

    void setListener(SelectChatListener listener) {
        this.listener = listener;
    }

    @Override
    public void callback(Message message) {
        if (message instanceof UserList) {
            listener.showUsers((UserList) message);
        }
    }
}
