/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.chat;

import ru.nsu.ccfit.priem.messages.ChatMessage;

public interface ChatListener {

    void showMyTextMessage(ChatMessage message);

    void showNotMyTextMessage(ChatMessage message);

    void goToSelectChat(int id);

}
