/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.chat;

import ru.nsu.ccfit.priem.messages.ChatMessage;

class TextMessageMaker {

    private Chat chat;

    TextMessageMaker(Chat chat) {
        this.chat = chat;
    }

    void createMessage(String text) {
        createMessage(text, "");
    }

    void createMessage(String text, String content) {
        ChatMessage newMessage = new ChatMessage(chat.getMyID(), chat.getNotMyID(), text, content);
        chat.sendMessage(newMessage);
        chat.getListener().showMyTextMessage(newMessage);
    }

}
