/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.clientIO;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import ru.nsu.ccfit.priem.crypto.Decoder;
import ru.nsu.ccfit.priem.crypto.Encoder;
import ru.nsu.ccfit.priem.messages.*;

import java.io.*;
import java.util.Properties;

public class ClientIO {

    private static volatile InternalLogger logger = Slf4JLoggerFactory.getInstance(ClientIO.class);

    private String IP;
    private int serverPort;
    private ClientIOListener clientIOListener;
    private volatile static ClientIO instance;
    private static final Object lock = new Object();
    private EventLoopGroup workerGroup;
    private Bootstrap b;
    private FtpClient ftpClient = new FtpClient();

    private ClientIO() {
        workerGroup = new NioEventLoopGroup();
        b = new Bootstrap();
        b.group(workerGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
    }

    public void bind(String IP, int port) {
        this.IP = IP;
        this.serverPort = port;
    }

    public static ClientIO getDefault() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new ClientIO();
                }
            }
        }
        return instance;
    }


    public void setClientIOListener(ClientIOListener chatClientIOListener) {
        this.clientIOListener = chatClientIOListener;
    }

    public void send(Message message) {
        try {
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch)
                        throws Exception {
                    ch.pipeline().addLast(new Encoder(),
                            new Decoder(), new ClientHandler(message));
                }
            });

            ChannelFuture f = b.connect(this.IP, this.serverPort).sync();

            f.channel().closeFuture();
        } catch (InterruptedException e) {
            logger.info(e.getLocalizedMessage(), e);
            Thread.currentThread().interrupt();
            workerGroup.shutdownGracefully();
        }
    }

    public File getFile(String fileName) {
        try {
            ftpClient.open();
            fileName = ftpClient.downloadFile(fileName);
            ftpClient.close();
        } catch (IOException e) {
            logger.info(e.getLocalizedMessage(), e);
        }
        return new File(fileName);
    }

    public String sendFile(File file) {
        try {
            ftpClient.open();
            ftpClient.putFileToPath(file);
            ftpClient.close();
        } catch (IOException e) {
            logger.debug(e.getLocalizedMessage(), e);
        }
        return file.getPath();
    }

    public class ClientHandler extends ChannelInboundHandlerAdapter {

        private Message message;

        ClientHandler (Message message) {
            super();
            this.message = message;
        }

        @Override
        public void channelActive(ChannelHandlerContext ctx)
                throws Exception {
            ctx.writeAndFlush(this.message);
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg)
                throws Exception {
            clientIOListener.callback((Message)msg);
            ctx.close();
        }
    }

    class FtpClient {

        private String server;
        private int port = 21;
        private String user;
        private String password;
        private FTPClient ftp;

        FtpClient() {
            Properties property = new Properties();
            try (FileInputStream fis = new FileInputStream("config.properties")) {
                property.load(fis);
                server = property.getProperty("ftp.ip");
                port = 21;
                user = "priem";
                password = property.getProperty("ftp.password");
            } catch (IOException e) {
                logger.info(e.getLocalizedMessage(), e);
            }
        }

        void open() throws IOException {
            ftp = new FTPClient();

            ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

            ftp.connect(server, port);
            int reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                throw new IOException("Exception in connecting to FTP Server");
            }

            ftp.login(user, password);
        }

        void close() throws IOException {
            ftp.disconnect();
        }

        String downloadFile(String fileName) throws IOException {
            String localFileName = "downloads" + fileName.substring(fileName.lastIndexOf("\\"));
            FileOutputStream out = new FileOutputStream(localFileName);
            ftp.retrieveFile(fileName, out);
            return localFileName;
        }

        void putFileToPath(File file) throws IOException {
            ftp.storeFile(file.getPath(), new FileInputStream(file));
        }

    }

}
