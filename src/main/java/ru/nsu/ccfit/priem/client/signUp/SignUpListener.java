/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.signUp;

public interface SignUpListener {

    void printError(String error);

    void goToSignIn();

}
