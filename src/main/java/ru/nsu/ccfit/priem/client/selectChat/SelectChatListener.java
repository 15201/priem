/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.client.selectChat;

import ru.nsu.ccfit.priem.messages.UserList;

public interface SelectChatListener {

    void showUsers(UserList chats);

    void goToChat(int idMy, int idNotMy);

    void goToSettings(int id);

}
