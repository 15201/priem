/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.RequestUser;
import ru.nsu.ccfit.priem.messages.UserList;

class GetUserHandler {

    UserList getUsers(RequestUser requestUser) {
        return DatabaseWorker.getDefault().getUsers(requestUser);
    }

}
