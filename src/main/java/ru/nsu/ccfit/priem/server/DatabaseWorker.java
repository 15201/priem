/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.crypto.EncodePass;
import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.ChangePasswordMessage;
import ru.nsu.ccfit.priem.messages.ChatMessage;
import ru.nsu.ccfit.priem.messages.MessageList;
import ru.nsu.ccfit.priem.messages.RequestMessage;
import ru.nsu.ccfit.priem.messages.RequestUser;
import ru.nsu.ccfit.priem.messages.SearchRequest;
import ru.nsu.ccfit.priem.messages.SignInInfo;
import ru.nsu.ccfit.priem.messages.SignUpInfo;
import ru.nsu.ccfit.priem.messages.User;
import ru.nsu.ccfit.priem.messages.UserList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

final class DatabaseWorker {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(DatabaseWorker.class);

    private EmailSender emailSender = new EmailSender();
    private Connection con;
    private static volatile DatabaseWorker instance;
    private static final Object lock = new Object();

    private DatabaseWorker() {
        try {
            con = DriverManager.getConnection("jdbc:postgresql://188.226.173.162:5432/priem", "priem", "p1r2i3e4m5");
        } catch (SQLException e) {
            logger.info(e.getLocalizedMessage(), e);
        }
    }

    public static DatabaseWorker getDefault() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new DatabaseWorker();
                }
            }
        }
        return instance;
    }

    synchronized Acknowledgement checkSignUpInfo(SignUpInfo signUpNote) {
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM users WHERE login=\'" +
                signUpNote.getLogin() + "\' OR email=\'" + signUpNote.getEmail() + "\'");
             ResultSet rs = pst.executeQuery()) {
            if (rs.next()) {
                return new Acknowledgement("ERROR: user already exists", false);
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        String password = UUID.randomUUID().toString();
        EncodePass encodePass = new EncodePass();
        try (PreparedStatement pst = con.prepareStatement("INSERT INTO users (login, password, email, last_time_online) VALUES (\'" + signUpNote.getLogin() + "\', \'" + encodePass.encode(password) + "\', \'" + signUpNote.getEmail() + "\', NOW())")) {
            pst.executeUpdate();
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        logger.debug("sending email to " + signUpNote.getEmail());
        emailSender.sendEmail(signUpNote, password);
        logger.debug("sent");
        return new Acknowledgement("", true);
    }

    synchronized Acknowledgement checkSignInInfo(SignInInfo signInNote) {
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM users WHERE login=\'" + signInNote.getLogin() + "\'");
             ResultSet rs = pst.executeQuery()) {
            if (rs.next()) {
                if (!rs.getString("password").equals(signInNote.getPassword())) {
                    return new Acknowledgement("ERROR: wrong password", false);
                } else {
                    return new Acknowledgement(Integer.toString(rs.getInt("id")), true);
                }
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new Acknowledgement("ERROR: wrong login", false);
    }

    synchronized Acknowledgement changePassword(ChangePasswordMessage changePasswordNote) {
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM users WHERE id=" + changePasswordNote.getIdSender());
             ResultSet rs = pst.executeQuery()) {
            if (rs.next()) {
                if (rs.getString("password").equals(changePasswordNote.getOldPassword())) {
                    try (PreparedStatement pst2 = con.prepareStatement("UPDATE users SET password=\'" + changePasswordNote.getNewPassword() + "\' WHERE id=" + changePasswordNote.getIdSender())) {
                        pst2.executeUpdate();
                        return new Acknowledgement("", true);
                    } catch (SQLException ex) {
                        logger.info(ex.getLocalizedMessage(), ex);
                    }
                } else {
                    return new Acknowledgement("ERROR: Wrong Password", false);
                }
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new Acknowledgement("ERROR: Wrong Id", false);
    }

    synchronized UserList searchUsers(SearchRequest searchNote) {
        List<User> userList = new LinkedList<>();
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM users WHERE login LIKE \'%" + searchNote.getSearchRequest() + "%\'");
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                userList.add(new User(rs.getInt("id"), rs.getString("login")));
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new UserList(userList);
    }

    synchronized Acknowledgement addMessage(ChatMessage chatMessageNote) {
        try (PreparedStatement pst = con.prepareStatement("INSERT INTO messages (id_sender, id_reciever, text, file_path, time) VALUES (" +
                chatMessageNote.getIdSender() + ", " + chatMessageNote.getIdReciever() + ", \'" + chatMessageNote.getText() + "\', \'" + chatMessageNote.getContent() + "\', NOW())")) {
            pst.executeUpdate();
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new Acknowledgement("", true);
    }

    synchronized MessageList getMessages(RequestMessage requestMessage) {
        List<ChatMessage> messageList = new LinkedList<>();
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM messages WHERE (id_sender=" + requestMessage.getId()
                + " AND id_reciever=" + requestMessage.getNotMyId() + ") OR (id_reciever=" + requestMessage.getId() + "AND " +
                "id_sender=" + requestMessage.getNotMyId() + ")");
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                if (requestMessage.isHistory() || (!rs.getBoolean("recieved") && (rs.getInt("id_sender") !=
                        requestMessage.getId()))) {
                    messageList.add(new ChatMessage(rs.getInt("id_sender"), rs.getInt("id_reciever"),
                            rs.getString("text"), rs.getString("file_path")));
                    try (PreparedStatement pst2 = con.prepareStatement("UPDATE messages SET recieved=TRUE WHERE id=" +
                            rs.getInt("id"))) {
                        pst2.executeUpdate();
                    } catch (SQLException ex) {
                        logger.info(ex.getLocalizedMessage(), ex);
                    }
                }
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new MessageList(messageList);
    }

    synchronized UserList getUsers(RequestUser requestUser) {
        List<User> userList = new LinkedList<>();
        try (PreparedStatement pst = con.prepareStatement("SELECT * FROM users WHERE id <> " + requestUser.getId());
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                userList.add(new User(rs.getInt("id"), rs.getString("login")));
            }
        } catch (SQLException ex) {
            logger.info(ex.getLocalizedMessage(), ex);
        }
        return new UserList(userList);
    }

}
