/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.ChangePasswordMessage;
import ru.nsu.ccfit.priem.messages.ChatMessage;
import ru.nsu.ccfit.priem.messages.Message;
import ru.nsu.ccfit.priem.messages.MessageList;
import ru.nsu.ccfit.priem.messages.RequestMessage;
import ru.nsu.ccfit.priem.messages.RequestUser;
import ru.nsu.ccfit.priem.messages.SearchRequest;
import ru.nsu.ccfit.priem.messages.SignInInfo;
import ru.nsu.ccfit.priem.messages.SignUpInfo;
import ru.nsu.ccfit.priem.messages.UserList;

class Handler {

    //TODO: FACTORY

    private SignUpHandler signUpHandler = new SignUpHandler();
    private SignInHandler signInHandler = new SignInHandler();
    private ChangePasswordHandler changePasswordHandler = new ChangePasswordHandler();
    private SearchEngine searchEngine = new SearchEngine();
    private ChatMessageHandler chatMessageHandler = new ChatMessageHandler();
    private GetMessageHandler getMessageHandler = new GetMessageHandler();
    private GetUserHandler getUserHandler = new GetUserHandler();

    private Acknowledgement handle(SignUpInfo signUpInfo) {
        return signUpHandler.checkSignUpInfo(signUpInfo);
    }

    private Acknowledgement handle(SignInInfo signInInfo) {
        return signInHandler.checkSignInInfo(signInInfo);
    }

    private Acknowledgement handle(ChangePasswordMessage changePasswordMessage) {
        return changePasswordHandler.changePassword(changePasswordMessage);
    }

    private UserList handle(SearchRequest searchRequest) {
        return searchEngine.search(searchRequest);
    }

    private Acknowledgement handle(ChatMessage chatMessage) {
        return chatMessageHandler.pass(chatMessage);
    }

    private MessageList handle(RequestMessage requestMessage) {
        return getMessageHandler.getMessages(requestMessage);
    }

    private UserList handle(RequestUser requestUser) {
        return getUserHandler.getUsers(requestUser);
    }

    Message handle(Message message) {
        if (message instanceof SignUpInfo) {
            return handle((SignUpInfo) message);
        }
        if (message instanceof SignInInfo) {
            return handle((SignInInfo) message);
        }
        if (message instanceof ChangePasswordMessage) {
            return handle((ChangePasswordMessage) message);
        }
        if (message instanceof SearchRequest) {
            return handle((SearchRequest) message);
        }
        if (message instanceof ChatMessage) {
            return handle((ChatMessage) message);
        }
        if (message instanceof RequestMessage) {
            return handle((RequestMessage) message);
        }
        if (message instanceof RequestUser) {
            return handle((RequestUser) message);
        }
        return null;
    }

}
