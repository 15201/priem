/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.ChatMessage;

class ChatMessageHandler {

    Acknowledgement pass(ChatMessage message) {
        return DatabaseWorker.getDefault().addMessage(message);
    }

}
