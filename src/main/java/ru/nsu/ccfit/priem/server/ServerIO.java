/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.crypto.Decoder;
import ru.nsu.ccfit.priem.crypto.Encoder;
import ru.nsu.ccfit.priem.messages.Message;

class ServerIO implements Runnable {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(ServerIO.class);

    private int port;
    private Handler handler;

    ServerIO(int port) {
        this.port = port;
        handler = new Handler();
    }

    @Override
    public void run() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline().addLast(new Decoder(),
                                    new Encoder(),
                                    new Reciever());
                        }
                    }).option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
            logger.debug("server is up");
        } catch (InterruptedException e) {
            logger.debug(e.getLocalizedMessage());
            Thread.currentThread().interrupt();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    private class Reciever extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            logger.debug("get");
            Message requestData = (Message) msg;
            Message responseData = handler.handle(requestData);
            ChannelFuture future = ctx.writeAndFlush(responseData);
            future.addListener(ChannelFutureListener.CLOSE);
            logger.debug(requestData.toString());
        }
    }

}
