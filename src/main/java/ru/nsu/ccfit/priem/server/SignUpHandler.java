/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.SignUpInfo;

class SignUpHandler {

    Acknowledgement checkSignUpInfo(SignUpInfo signUpInfo) {
        return DatabaseWorker.getDefault().checkSignUpInfo(signUpInfo);
    }

}
