/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.MessageList;
import ru.nsu.ccfit.priem.messages.RequestMessage;

class GetMessageHandler {

    MessageList getMessages(RequestMessage requestMessage) {
        return DatabaseWorker.getDefault().getMessages(requestMessage);
    }

}
