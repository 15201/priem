/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.ChangePasswordMessage;

class ChangePasswordHandler {

    Acknowledgement changePassword(ChangePasswordMessage changePasswordMessage) {
        return DatabaseWorker.getDefault().changePassword(changePasswordMessage);
    }

}
