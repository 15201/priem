/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import ru.nsu.ccfit.priem.messages.SignUpInfo;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class EmailSender {

    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(EmailSender.class);

    void sendEmail(SignUpInfo signUpInfo, String pass) {
        Properties property = new Properties();
        try (FileInputStream fis = new FileInputStream("config.properties")) {
            property.load(fis);
        } catch (IOException e) {
           logger.info(e.getLocalizedMessage(), e);
        }

        final String username = "priemover@gmail.com";
        final String password = property.getProperty("email.password");
        Properties props = new Properties();
        props.put("mail.host", "smtp.gmail.com");
        props.put("mail.defaultEncoding", "UTF-8");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(signUpInfo.getEmail()));
            message.setSubject("Приём: вы зарегистрировались");
            message.setText("Здравствуйте, поздравляем вас с успешной регистрацией, вот ваш пароль: " + pass);
            Transport.send(message);
        } catch (MessagingException e) {
            logger.info(e.getLocalizedMessage(), e);
        }
    }

}
