/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.Acknowledgement;
import ru.nsu.ccfit.priem.messages.SignInInfo;

class SignInHandler {

    Acknowledgement checkSignInInfo(SignInInfo signInInfo) {
        return DatabaseWorker.getDefault().checkSignInInfo(signInInfo);
    }

}
