/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import io.netty.util.internal.logging.InternalLogger;
import io.netty.util.internal.logging.Slf4JLoggerFactory;

public class Server {
    private static final InternalLogger logger = Slf4JLoggerFactory.getInstance(Server.class);

    private Server () {

    }

    public static void main(String[] args) {
        logger.debug("running");
        int port = 9000;
        new Thread(new ServerIO(port)).start();
    }

}


