/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.server;

import ru.nsu.ccfit.priem.messages.SearchRequest;
import ru.nsu.ccfit.priem.messages.UserList;

class SearchEngine {

    UserList search(SearchRequest searchRequest) {
        return DatabaseWorker.getDefault().searchUsers(searchRequest);
    }

}
