/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class ChatMessage implements Message {

    private final String type = "ChatMessage";
    private int idSender;
    private int idReciever;
    private String text;
    private String content;

    public ChatMessage(int idSender, int idReciever, String text, String content) {
        this.idSender = idSender;
        this.idReciever = idReciever;
        this.content = content;
        this.text = text;
    }

    public String getContent() {
        return content;
    }

    public int getIdReciever() {
        return idReciever;
    }

    public int getIdSender() {
        return idSender;
    }

    public String getText() {
        return text;
    }
}
