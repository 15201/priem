/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class SignUpInfo implements Message {

    private final String type = "SignUpInfo";
    private String login;
    private String email;

    public SignUpInfo(String login, String email) {
        this.login = login;
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

}
