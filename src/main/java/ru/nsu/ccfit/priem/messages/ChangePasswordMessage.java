/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class ChangePasswordMessage implements Message {

    private final String type = "ChangePasswordMessage";
    private int idSender;
    private String oldPassword;
    private String newPassword;

    public ChangePasswordMessage(int idSender, String oldPassword, String newPassword) {
        this.idSender = idSender;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public int getIdSender() {
        return idSender;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }
}
