/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

import java.util.ArrayList;
import java.util.List;

public class MessageList implements Message {

    private final String type = "MessageList";
    private List<ChatMessage> messageList;

    public MessageList(List<ChatMessage> messages) {
        this.messageList = new ArrayList<>(messages);
    }

    public List<ChatMessage> getMessageList() {
        return new ArrayList<>(messageList);
    }

}
