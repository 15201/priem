/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class User {

    private final String type = "User";
    private int id;
    private String login;

    public User(int id, String login) {
        this.id = id;
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }
}
