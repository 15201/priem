/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class RequestUser implements Message {

    private final String type = "RequestUser";
    private int id;

    public RequestUser(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
