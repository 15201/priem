/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

import java.util.ArrayList;
import java.util.List;

public class UserList implements Message {

    private final String type = "UserList";
    private List<User> userList;

    public UserList(List<User> users) {
        this.userList = new ArrayList<>(users);
    }

    public List<User> getUserList() {
        return new ArrayList<>(userList);
    }
}
