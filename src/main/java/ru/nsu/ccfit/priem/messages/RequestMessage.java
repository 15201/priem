/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class RequestMessage implements Message {

    private final String type = "requestMessage";
    private int id;
    private int notMyId;
    private boolean history;

    public RequestMessage(int id, int notMyId, boolean history) {
        this.id = id;
        this.notMyId = notMyId;
        this.history = history;
    }

    public int getId() {
        return id;
    }

    public int getNotMyId() {
        return notMyId;
    }

    public boolean isHistory() {
        return history;
    }
}
