/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class SearchRequest implements Message {

    private final String type = "SearchRequest";
    private String searchRequest;

    public SearchRequest(String searchRequest) {
        this.searchRequest = searchRequest;
    }

    public String getSearchRequest() {
        return searchRequest;
    }
}
