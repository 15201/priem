/*
 * Copyright (C) 2018 Priem
 */

package ru.nsu.ccfit.priem.messages;

public class Acknowledgement implements Message {

    private final String type = "Acknowledgement";
    private String answer;
    private boolean ack;

    public Acknowledgement(String answer, boolean ack) {
        this.ack = ack;
        this.answer = answer;
    }

    public boolean isAck() {
        return ack;
    }

    public String getAnswer() {
        return answer;
    }

}
