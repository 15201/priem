package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.SearchRequest;

public class SearchRequestTest {
    private SearchRequest searchRequest;

    @Before
    public void init(){
        searchRequest = new SearchRequest("name_chat");
    }

    @After
    public void tearDown(){
        searchRequest = null;
    }

    @Test
    public void searchRequest(){
        Assert.assertEquals("searchRequest should have been equals name_chat", "name_chat", searchRequest.getSearchRequest());
    }
}
