package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.RequestMessage;

public class RequestMessageTest {
    private RequestMessage requestMessage;

    @Before
    public void init(){
        requestMessage = new RequestMessage(10, 11, true);
    }

    @After
    public void tearDown(){
        requestMessage = null;
    }

    @Test
    public void id(){
        Assert.assertEquals("check myID RequestMessage", 10, requestMessage.getId());
    }

    @Test
    public void notMyId() {
        Assert.assertEquals("check notMyID RequestMessage", 11, requestMessage.getNotMyId());
    }

    @Test
    public void history(){
        Assert.assertTrue("check history RequestMessage", requestMessage.isHistory());
    }
}
