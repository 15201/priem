package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.User;
import ru.nsu.ccfit.priem.messages.UserList;

import java.util.LinkedList;
import java.util.List;

public class UserListTest {
    private UserList userList;
    private List<User> users;

    @Before
    public void init(){
        users = new LinkedList<>();
        users.add(new User(0, "name0"));
        users.add(new User(1, "name1"));
        users.add(new User(2, "name2"));
        userList = new UserList(users);
    }

    @After
    public void tearDown(){
        userList = null;
        users = null;
    }

    @Test
    public void userList(){
        Assert.assertEquals("There should have been 3 users in the list", 3, userList.getUserList().size());
        Assert.assertEquals("check third element and his login", "name2", userList.getUserList().get(2).getLogin());
        Assert.assertEquals("check second element and his id", 1, userList.getUserList().get(1).getId());
    }
}
