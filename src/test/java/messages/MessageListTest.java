package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.ChatMessage;
import ru.nsu.ccfit.priem.messages.MessageList;

import java.util.LinkedList;
import java.util.List;

public class MessageListTest {
    private MessageList messageList;
    private List<ChatMessage> messages;

    @Before
    public void init(){
        messages = new LinkedList<>();
        messages.add(new ChatMessage(4, 6, "hello", "22"));
        messages.add(new ChatMessage(5, 8, "What's up?", "100"));
        messageList = new MessageList(messages);
    }

    @After
    public void tearDown(){
        messageList = null;
        messages = null;
    }

    @Test
    public void messageList(){
        Assert.assertEquals("There should have been 2 messages in the list", 2, messageList.getMessageList().size());
        Assert.assertEquals("check first element and his idReceiver", 6, messageList.getMessageList().get(0).getIdReciever());
        Assert.assertEquals("check first element and his content", "22", messageList.getMessageList().get(0).getContent());
        Assert.assertEquals("check second element and his idSender", 5, messageList.getMessageList().get(1).getIdSender());
        Assert.assertEquals("check second element and his text", "What's up?", messageList.getMessageList().get(1).getText());
    }
}
