package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.SignInInfo;

public class SignInInfoTest {
    private SignInInfo signInInfo;

    @Before
    public void init(){
        signInInfo = new SignInInfo("name0", "QwEr01Ty#");
    }

    @After
    public void tearDown(){
        signInInfo = null;
    }

    @Test
    public void password(){
        Assert.assertEquals("check password SignInInfo", "QwEr01Ty#", signInInfo.getPassword());
    }

    @Test
    public void login(){
        Assert.assertEquals("check login SignInInfo", "name0", signInInfo.getLogin());
    }
}
