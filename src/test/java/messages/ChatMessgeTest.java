package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.ChatMessage;

public class ChatMessgeTest {
    private ChatMessage chatMessage;

    @Before
    public void init(){
        chatMessage = new ChatMessage(10, 12, "hello", "content");
    }

    @After
    public void tearDown(){
        chatMessage = null;
    }

    @Test
    public void idSender(){
        Assert.assertEquals("idSender should have been 10", 10, chatMessage.getIdSender());
    }

    @Test
    public void idReceiver(){
        Assert.assertEquals("idReceiver should have been 12", 12, chatMessage.getIdReciever());
    }

    @Test
    public void content() {
        Assert.assertEquals("content should have been equal content", "content", chatMessage.getContent());
    }

    @Test
    public void text() {
        Assert.assertEquals("text should have been equal hello", "hello", chatMessage.getText());
    }
}
