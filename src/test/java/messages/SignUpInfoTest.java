package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.SignUpInfo;

public class SignUpInfoTest {
    private SignUpInfo signUpInfo;

    @Before
    public void init(){
        signUpInfo = new SignUpInfo("name0", "name0@mail.ru");
    }

    @After
    public void tearDown(){
        signUpInfo = null;
    }

    @Test
    public void email(){
        Assert.assertEquals("check e-mail SignUpInfo", "name0@mail.ru", signUpInfo.getEmail());
    }

    @Test
    public void login(){
        Assert.assertEquals("check login SignUpInfo", "name0", signUpInfo.getLogin());
    }
}
