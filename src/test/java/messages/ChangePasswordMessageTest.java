package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.ChangePasswordMessage;

public class ChangePasswordMessageTest {
    private ChangePasswordMessage passwordMessage;

    @Before
    public void init(){
        passwordMessage = new ChangePasswordMessage(10, "qwerty", "QwEr1t#");
    }

    @After
    public void tearDown(){
        passwordMessage = null;
    }

    @Test
    public void id(){
        Assert.assertEquals("idSender should have been 10", 10, passwordMessage.getIdSender());
    }

    @Test
    public void oldPassword(){
        Assert.assertEquals("oldPassword should have been qwerty", "qwerty", passwordMessage.getOldPassword());
    }

    @Test
    public void newPassword(){
        Assert.assertEquals("newPassword should have been QwEr1t#", "QwEr1t#", passwordMessage.getNewPassword());
    }
}
