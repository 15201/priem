package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.Acknowledgement;

public class AcknowledgementTest {
    private Acknowledgement acknowledgement;

    @Before
    public void init(){
        acknowledgement = new Acknowledgement("ack", false);
    }

    @After
    public void tearDown(){
        acknowledgement = null;
    }

    @Test
    public void answer(){
        Assert.assertEquals("answer should have been ack","ack", acknowledgement.getAnswer());
    }

    @Test
    public void ack(){
        Assert.assertFalse("ack should have been false", acknowledgement.isAck());
    }
}
