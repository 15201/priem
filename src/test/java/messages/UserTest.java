package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.User;

public class UserTest {
    private User user;

    @Before
    public void init(){
        user = new User(0, "name0");
    }

    @After
    public void tearDown(){
        user = null;
    }

    @Test
    public void id(){
        Assert.assertEquals("check id User", 0, user.getId());
    }

    @Test
    public void login(){
        Assert.assertEquals("check login User", "name0", user.getLogin());
    }
}
