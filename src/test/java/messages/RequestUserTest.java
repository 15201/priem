package messages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.messages.RequestUser;

public class RequestUserTest {
    private RequestUser requestUser;

    @Before
    public void init(){
        requestUser = new RequestUser(10);
    }

    @After
    public void tearDown(){
        requestUser = null;
    }

    @Test
    public void id(){
        Assert.assertEquals("check ID user RequestUser", 10, requestUser.getId());
    }
}
