package client.chat;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.client.chat.Chat;
import ru.nsu.ccfit.priem.client.chat.ChatListener;
import ru.nsu.ccfit.priem.messages.ChatMessage;

public class ChatTest {
    private Chat chat;

    @Before
    public void init() {
        chat = new Chat(5, 0);
    }

    @After
    public void tearDown() {
        chat = null;
    }

    @Test
    public void myID() {
        Assert.assertEquals("myID should have been 5 Chat", 5, chat.getMyID());
    }

    @Test
    public void notMyID() {
        Assert.assertEquals("notMyID should have been 0 Chat", 0, chat.getNotMyID());
    }

    @Test
    public void getSetListener() {
        ChatListener chatListener = new ChatListener() {
            @Override
            public void showMyTextMessage(ChatMessage message) {

            }

            @Override
            public void showNotMyTextMessage(ChatMessage message) {

            }

            @Override
            public void goToSelectChat(int id) {

            }
        };
        chat.setListener(chatListener);
        Assert.assertEquals("check setListener & getListener Chat", chatListener, chat.getListener());
    }
}