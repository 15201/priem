package client.chat;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.ccfit.priem.client.chat.ExtensionFileFilter;

import java.io.File;

public class ExtensionFileFilterTest {
    private ExtensionFileFilter fileFilter;

    @Before
    public void init(){
        fileFilter = new ExtensionFileFilter("png", "rastr file");
    }

    @After
    public void tearDown(){
        fileFilter = null;
    }

    @Test
    public void description(){
        Assert.assertEquals("check getDescription FileFilter","rastr file (*.png)", fileFilter.getDescription());
    }

    @Test
    public void accept(){
        Assert.assertTrue("check accept FileFilter", fileFilter.accept(new File("picture.png")));
    }
}
