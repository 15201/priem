package client.chat;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.ccfit.priem.client.chat.FileUtils;

import java.io.File;

public class FileUtilsTest {

    @Test
    public void dataDirectory(){
        File directory =  new File(".");
        Assert.assertEquals("check getDataDirectory FileUtils", directory, FileUtils.getDataDirectory());
    }

}
